#!/bin/bash
set -e
echo "Starting of Bootstrap File - Flight Announcements"

# Load context into environment
. /root/bamboo-vars.conf
. /root/context

useradd tomcat
echo "Added user tomcat"
 
AppDynamicsAppName=Flight_Information
enable_appdynamics="false"
BUCKET=pipeline-artefact-store/common
JdkLinuxVersion=jdk-8u112-linux-x64
JdkInstallDir=jdk1.8.0_112
AppDynamicsVersion=4.1.10.0
AppDynamicsControllerHost="qantas-nonprod.saas.appdynamics.com";
AppDynamicsAccountName="qantas-nonprod";
AppDynamicsAccessKey="e9e6872801d7";

# Identify environment type
if [[ $bamboo_planName == *"DEV"* ]]
then
  spring_profile="dev";
elif [[ $bamboo_planName == *"STG"* ]]
then
  enable_appdynamics="true"
  spring_profile="staging";
elif [[ $bamboo_planName == *"PROD"* ]]
then
  spring_profile="production";
  enable_appdynamics="true"
  AppDynamicsControllerHost="qantas-prod.saas.appdynamics.com";
  AppDynamicsAccountName="qantas-prod";
  AppDynamicsAccessKey="a6475993df1b";
else
  spring_profile="production";
fi

echo "Environment identification done"

unset HTTP_PROXY
unset HTTPS_PROXY
unset http_proxy
unset https_proxy

#Spring profile configuration
sed -i -e "s/%SPRING_PROFILE%/${spring_profile}/g" /root/payload/startup/app_start.sh
sed -i -e "s/%flt_announcement_redis_DeployDnsName%/${flt_announcement_redis_DeployDnsName}/g" /root/payload/startup/app_start.sh
sed -i -e "s/%flt_announcement_redis_MyCachePrimaryEndPointPort%/${flt_announcement_redis_MyCachePrimaryEndPointPort}/g" /root/payload/startup/app_start.sh


#Set java proxy
http_proxy=$(echo $bamboo_aws_proxy | cut -d'/' -f3)
proxy_host=$(echo $http_proxy | cut -d':' -f1)
proxy_port=$(echo $http_proxy | cut -d':' -f2)
sed -i -e "s/%PROXY_HOST%/${proxy_host}/g" /root/payload/startup/app_start.sh
sed -i -e "s/%PROXY_PORT%/${proxy_port}/g" /root/payload/startup/app_start.sh

echo "Setting java proxy done"
# AppDynamics Configuration settings
BRANCH="${bamboo_branch}-${bamboo_buildNumber}"
sed -i -e "s/%ENABLE_APPDYNAMICS%/${enable_appdynamics}/g" /root/payload/startup/app_start.sh
sed -i -e "s/%HOST_NAME%/${AppDynamicsControllerHost}/g" /root/payload/appdynamics/appserveragent/controller-info.xml
sed -i -e "s/%HOST_NAME%/${AppDynamicsControllerHost}/g" /root/payload/appdynamics/machineagent/controller-info.xml
sed -i -e "s/%ACCOUNT_NAME%/${AppDynamicsAccountName}/g" /root/payload/appdynamics/appserveragent/controller-info.xml
sed -i -e "s/%ACCOUNT_NAME%/${AppDynamicsAccountName}/g" /root/payload/appdynamics/machineagent/controller-info.xml
sed -i -e "s/%ACCESS_KEY%/${AppDynamicsAccessKey}/g" /root/payload/appdynamics/appserveragent/controller-info.xml
sed -i -e "s/%ACCESS_KEY%/${AppDynamicsAccessKey}/g" /root/payload/appdynamics/machineagent/controller-info.xml
sed -i -e "s/%TIER_NAME%/$BRANCH/g" /root/payload/appdynamics/appserveragent/controller-info.xml
sed -i -e "s/%TIER_NAME%/$BRANCH/g" /root/payload/appdynamics/machineagent/controller-info.xml
sed -i -e "s/%APP_DYNAMICS_VERSION%/$AppDynamicsVersion/g" /root/payload/appdynamics/machineagent/machineagent.initd
sed -i -e "s/%APP_DYNAMICS_VERSION%/$AppDynamicsVersion/g" /root/payload/startup/app_start.sh
sed -i -e "s/%APP_NAME%/$AppDynamicsAppName/g" /root/payload/appdynamics/machineagent/machineagent.initd
sed -i -e "s/%APP_NAME%/$AppDynamicsAppName/g" /root/payload/appdynamics/machineagent/controller-info.xml
sed -i -e "s/%APP_NAME%/$AppDynamicsAppName/g" /root/payload/appdynamics/appserveragent/controller-info.xml

echo "Appdynamics configuration setup done"

#Splunk configuration settings
sed -i -e "s/%AMS_ID%/${bamboo_planKey}/g" /root/payload/splunk/inputs.conf
sed -i -e "s/%APPLICATION%/${bamboo_product}/g" /root/payload/splunk/inputs.conf
sed -i -e "s/%BRANCH%/${bamboo_branch}/g" /root/payload/splunk/inputs.conf
sed -i -e "s/%BUILDNO%/${bamboo_buildNumber}/g" /root/payload/splunk/inputs.conf
DEPLOYMENT_ENVIRONMENT=${bamboo_deployment_env}
case "$DEPLOYMENT_ENVIRONMENT" in
'NonProduction')
    sed -i -e "s/%SPLUNK_INDEX%/qcp_a951_nonprod/g" /root/payload/splunk/inputs.conf
    ;;
'Production')
    sed -i -e "s/%SPLUNK_INDEX%/qcp_a951_prod/g" /root/payload/splunk/inputs.conf
    ;;

'*')
    echo "Could not replace splunk input config"
    exit 1;
    ;;
esac

echo "Splunk configuration setup done"

# Secondary Volume Setup
MOUNT_POINT="/data"
DEVICE=""

# Locate device (should be at /dev/sdc but could differ based on kernel type)
lsblk /dev/sdp && DEVICE="/dev/sdp"
lsblk /dev/xvdp && DEVICE="/dev/xvdp"

mkdir -p "$MOUNT_POINT"

if [ ! -z "$DEVICE" ]
then

# Setup mount for bootstrap
mkfs -t ext4 "$DEVICE"
mount "$DEVICE" "$MOUNT_POINT"

# Setup for next boot
cat - <<EOF >>/etc/fstab

## Added by bootstrap script on `date`
$DEVICE ${MOUNT_POINT/ /\\ } ext4 defaults 0 2
##
EOF
fi

echo "Mounting of disk completed"

## Add entry to hosts file to resolve ip-* to instance IP (Current bake instance)
if [[ -n "$(grep $(hostname) /etc/hosts)" ]]; then echo hostname present ; else  echo "$(hostname -s | cut -d '-' -f 2- | sed 's8-8.8g') $(hostname -s)" | tee -a /etc/hosts ; fi

## Add line to rc.local to add entry to hosts file to resolve ip-* to instance IP (Future instances in behind autoscale group)
echo "if [[ -n \"\$(grep \$(hostname) /etc/hosts)\" ]]; then echo hostname present ; else  echo \"\$(hostname -s | cut -d '-' -f 2- | sed 's8-8.8g') \$(hostname -s)\" >> /etc/hosts ; fi " | tee -a /etc/rc.local


## Install JDK
/usr/local/bin/aws s3 cp s3://$BUCKET/jdk/$JdkLinuxVersion.rpm /tmp/$JdkLinuxVersion.rpm
rpm -ivh /tmp/$JdkLinuxVersion.rpm
alternatives --set java /usr/java/$JdkInstallDir/jre/bin/java

echo "Java installation completed"
yum install unzip -y
if [ ${enable_appdynamics} == true ]; then
  ## Unpack AppDynamics Files.

  AppServerAgent="AppServerAgent-$AppDynamicsVersion.zip"
  MachineAgent="MachineAgent-$AppDynamicsVersion.zip"

  /usr/local/bin/aws s3 cp s3://$BUCKET/appdynamics/v$AppDynamicsVersion/$AppServerAgent .
  /usr/local/bin/aws s3 cp s3://$BUCKET/appdynamics/v$AppDynamicsVersion/$MachineAgent .

  mkdir -p /opt/AppDynamics

  unzip $MachineAgent -d /opt/AppDynamics/MachineAgent
  unzip $AppServerAgent -d /opt/AppDynamics/AppServerAgent

  ## Configure AppDynamics MachineAgent.

  cp /root/payload/appdynamics/machineagent/machineagent.initd /etc/init.d/machineagent
  cp /root/payload/appdynamics/machineagent/controller-info.xml /opt/AppDynamics/MachineAgent/conf
  chmod +x /etc/init.d/machineagent

  ## Configure AppDynamics AppServerAgent.
  mkdir -p /opt/AppDynamics/AppServerAgent/ver$AppServerAgent/logs/$HOSTNAME
  cp /root/payload/appdynamics/appserveragent/controller-info.xml /opt/AppDynamics/AppServerAgent/ver$AppDynamicsVersion/conf

  chkconfig --add machineagent
  chkconfig machineagent on

  chown -R tomcat:tomcat /opt/AppDynamics
fi

echo "App dynamics setup completed"

#Configure Splunk
mkdir -p /opt/splunkforwarder/etc/apps/qcom/default/
cp /root/payload/splunk/inputs.conf /opt/splunkforwarder/etc/apps/qcom/default/inputs.conf

mv /root/payload/app/*.war /home/tomcat/
mv /root/payload/startup/app_start.sh /home/tomcat/

echo "Creating application startup file"
echo "@reboot /home/tomcat/app_start.sh" >> tmpcron
crontab -u tomcat tmpcron

echo "Add permissions to startup file"
chown -R tomcat:tomcat /home/tomcat/
chmod -R 0755 /home/tomcat
chmod +x /home/tomcat/app_start.sh
chmod +x /home/tomcat/*.war


echo "Spring boot startup file created"

mkdir -p /data/logs/flightannouncements
chmod -R 0755 /data/logs/flightannouncements
chown -R tomcat:tomcat /data
chown -R tomcat:tomcat /data/logs/flightannouncements

echo "End of Bootstrap File - Flight Announcements"