#!/bin/bash
set -e

echo "Running Flight Announcements build.sh script"

echo "===== Environment Variables ====="
env | grep -v -i password
echo "=========="

# Build application package
cd $APP_DIR && mvn clean package -Dmaven.test.skip=true && cd ..

echo "Moving application artefact to payload directory"

# Setup payload for upload
mkdir -p $PAYLOAD_DIR/app
cp -r $APP_DIR/target/*.war $PAYLOAD_DIR/app

echo "Moving application scripts contents to payload directory"
cp -r $COMPONENT_DIR/scripts/* $PAYLOAD_DIR/

echo "Moving application bootstrap to payload directory"
cp -r $COMPONENT_DIR/bootstrap.sh $PAYLOAD_DIR/

echo "Listing payload contents"
ls -R $PAYLOAD_DIR

exit 0