#!/usr/bin/env bash
export HTTP_PROXY=http://proxy.qcpaws.qantas.com.au:3128
export http_proxy=http://proxy.qcpaws.qantas.com.au:3128
# Set hostname for AppDynamics
NEWHOST="$HOSTNAME"
ENABLE_APPDYNAMICS=%ENABLE_APPDYNAMICS%
if [ $ENABLE_APPDYNAMICS == true ]; then
    JAVA_OPTS="$JAVA_OPTS -Dappdynamics.agent.nodeName=$NEWHOST -Dappdynamics.http.proxyHost=proxy.qcpaws.qantas.com.au -Dappdynamics.http.proxyPort=3128"
    JAVA_OPTS="$JAVA_OPTS -javaagent:/opt/AppDynamics/AppServerAgent/ver%APP_DYNAMICS_VERSION%/javaagent.jar"
fi
SPRING_PROFILE="%SPRING_PROFILE%"
REDIS_HOST="%flt_announcement_redis_DeployDnsName%"
REDIS_PORT="%flt_announcement_redis_MyCachePrimaryEndPointPort%"
if [ -z "$SPRING_PROFILE" ]; then
    SPRING_PROFILE="production"
fi
java $JAVA_OPTS -Dhttps.proxyHost=%PROXY_HOST% -Dhttps.proxyPort=%PROXY_PORT% -Dhttps.nonProxyHosts="*.qcpaws.qantas.com.au|www-staging.qantas.com.au|www.qantas.com.au" -jar /home/tomcat/*.war --spring.profiles.active=$SPRING_PROFILE --spring.redis.host=$REDIS_HOST --spring.redis.port=$REDIS_PORT
echo "$(date) Flight Announcements launched..., process list is $(ps -ef | grep -i java)"