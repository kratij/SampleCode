package com.qantas.flight.announcements;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.map.repository.config.EnableMapRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Slf4j
@SpringBootApplication
@ComponentScan({ "com.qantas.flight.announcements*" })
@EnableCaching
@EnableRedisRepositories
@EnableMapRepositories
public class Application {
    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
