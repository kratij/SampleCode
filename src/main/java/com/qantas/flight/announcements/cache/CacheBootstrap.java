package com.qantas.flight.announcements.cache;

import com.qantas.flight.announcements.component.StatesAirportsComponent;
import com.qantas.flight.announcements.component.RetrieveAnnouncements;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;

@Slf4j
@Component
public class CacheBootstrap implements CommandLineRunner, ApplicationListener<EmbeddedServletContainerInitializedEvent> {
    private int port;

    @Autowired
    private RetrieveAnnouncements retrieveAnnouncements;

    @Autowired
    private StatesAirportsComponent statesAirportsComponent;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
        port = event.getEmbeddedServletContainer().getPort();
    }

    @Override
    public void run(String... strings) throws Exception {
        try {
            Set<String> redisKeys = redisTemplate.keys("flifo*");
            if(!redisKeys.stream().findAny().isPresent()){
            retrieveAnnouncements.loadProperties();
            }
            statesAirportsComponent.getAirport();
        } catch (IOException ioe) {
            log.error("Error in reading the JSON file !!");
            throw ioe;
        }
    }

}