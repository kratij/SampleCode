package com.qantas.flight.announcements.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
@Component
public class AirportDetails {

    private static final String ORDINAL_INDICATOR_TH = "th";
    private static final String ORDINAL_INDICATOR_ST = "st";
    private static final String ORDINAL_INDICATOR_RD = "rd";
    private static final String ORDINAL_INDICATOR_ND = "nd";
    private static final Integer TEN = 10;
    private static final Integer ELEVEN = 11;
    private static final Integer THIRTEEN = 13;

    @Autowired
    StatesAirportsComponent statesAirportsComponent;

    public String retrieveTimezone(String port){

        return statesAirportsComponent.getFlifoAirports().getCountries().stream()
                .flatMap(state->state.getStates().stream().flatMap(city->city.getCities().stream()
                        .filter(c -> c.getCode().equalsIgnoreCase(port)))).findFirst().get().getTimezone();
        }

    public LocalDateTime retrieveLocalTime(String timezone){

        return LocalDateTime.now(ZoneId.of(timezone));
    }

    public String returnDayOfMonth(Integer dayOfMonth) {

        if (dayOfMonth >= ELEVEN && dayOfMonth <= THIRTEEN) {
            return (dayOfMonth.toString() + ORDINAL_INDICATOR_TH);
        } else {
            switch (dayOfMonth % TEN) {
                case 1:
                    return (dayOfMonth.toString() + ORDINAL_INDICATOR_ST);
                case 2:
                    return (dayOfMonth.toString() + ORDINAL_INDICATOR_ND);
                case 3:
                    return (dayOfMonth.toString() + ORDINAL_INDICATOR_RD);
                default:
                    return (dayOfMonth.toString() + ORDINAL_INDICATOR_TH);
            }
        }
    }
}
