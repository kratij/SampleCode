package com.qantas.flight.announcements.component;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.*;

import static net.logstash.logback.marker.Markers.append;

@NoArgsConstructor
@Slf4j
@Component
public class RetrieveAnnouncements {

    @Autowired
    private RedisTemplate redisTemplate;

    private static final String MESSAGES_PROPERTIES_PATH = "messages.properties";

    private static final String KEY = "Announcements";


    public void loadProperties() {

        try {

            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(MESSAGES_PROPERTIES_PATH)));
            } catch (Exception e) {
                e.printStackTrace();
            }

            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains("=")) {
                    String[] strings = line.split("=");
                    redisTemplate.opsForHash().put(KEY,strings[0].trim(),strings[1]);
                    }
            }

        } catch (IOException e) {
            log.error(append("Failed to load Announcement message property file", e), null);
        }
    }

    public String getProperty(String key) {
        if(redisTemplate==null){
            loadProperties();
        }

        if(redisTemplate.opsForHash().hasKey(KEY,key))
            return redisTemplate.opsForHash().get(KEY,key).toString();
        else
            return "";
     }


    public String setProperty(String key,String value) {
        if(redisTemplate==null){
            loadProperties();
        }

        redisTemplate.opsForHash().put(KEY,key,value);
        return "Update was successful";
    }

    public String deleteProperty(String key)
    {
        redisTemplate.opsForHash().delete(KEY,key);
        return "Delete was successful";
    }
}
