package com.qantas.flight.announcements.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qantas.flight.announcements.model.FlifoAirportDetails;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StatesAirportsComponent
{
    private static FlifoAirportDetails flifoAirportDetails;
    private static final String FILE = "states-airports.json";

    public void getAirport() throws IOException {

        flifoAirportDetails = new ObjectMapper().readValue(this.getClass().getClassLoader()
                .getResourceAsStream(FILE), FlifoAirportDetails.class);
    }

    public FlifoAirportDetails getFlifoAirports() {

        return flifoAirportDetails;
    }
}
