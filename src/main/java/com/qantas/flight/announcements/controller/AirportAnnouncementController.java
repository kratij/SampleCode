package com.qantas.flight.announcements.controller;

import com.qantas.flight.announcements.exceptions.ApplicationException;
import com.qantas.flight.announcements.model.FlightResponse;
import com.qantas.flight.announcements.service.AirportAnnouncementService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static net.logstash.logback.marker.Markers.append;

@RestController
@Slf4j
@AllArgsConstructor
public class AirportAnnouncementController {

    @Autowired
    private AirportAnnouncementService airportAnnouncementService;

    @ApiOperation(value = "Updates the airport based on port", httpMethod = "GET")
    @GetMapping(path = "/v1/airports/{port}/{messageCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightResponse retrieveAirportAnnouncements(@PathVariable("port") String port,
                                                       @PathVariable("messageCode") String messageCode,
                                                       @RequestParam(value = "travelType", required = false) String travelType,
                                                       @RequestParam(value = "channel", required = true) String channel) throws ApplicationException {
        log.info(append("messageCode", messageCode), null);

        return airportAnnouncementService.retrieveAirportAnnouncements(port,messageCode,travelType);
    }
}
