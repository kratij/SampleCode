package com.qantas.flight.announcements.controller;

import com.qantas.flight.announcements.exceptions.ApplicationException;
import com.qantas.flight.announcements.model.FlightResponse;
import com.qantas.flight.announcements.service.FlightDomAnnouncementService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static net.logstash.logback.marker.Markers.append;

@RestController
@Slf4j
@AllArgsConstructor
public class FlightDomAnnouncementController {

    private FlightDomAnnouncementService flightDomAnnouncementService;

    @ApiOperation(value = "Retrieves domestic announcements based on message code", httpMethod = "GET")
    @GetMapping(path = "/v1/{messageCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightResponse retrieveAnnouncements(@PathVariable("messageCode") String messageCode,
                                                @RequestParam(value = "traveltype", required = false) String traveltype,
                                                @RequestParam(value = "channel", required = true) String channel) throws ApplicationException {
        log.info(append("messageCode", messageCode), null);

        return flightDomAnnouncementService.retrieveDomAnnouncements(messageCode);
    }
}
