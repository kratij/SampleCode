package com.qantas.flight.announcements.controller;

import com.qantas.flight.announcements.exceptions.ApplicationException;
import com.qantas.flight.announcements.model.FlightResponse;
import com.qantas.flight.announcements.service.FlightIntlAnnouncementService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static net.logstash.logback.marker.Markers.append;


@RestController
@AllArgsConstructor
@Slf4j
public class FlightIntlAnnouncementController {

    @Autowired
    private FlightIntlAnnouncementService flightIntlAnnouncementService;

    @ApiOperation(value = "Retrieves international announcements based on message code", httpMethod = "GET")
    @GetMapping(path = "/v1/countries/{country}", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightResponse retrieveIntlAnnouncements(@PathVariable("country") String country,
                                                    @RequestParam(value = "traveltype", required = false) String traveltype,
                                                    @RequestParam(value = "channel", required = true) String channel) throws ApplicationException {
        log.info(append("country", country), null);

        return flightIntlAnnouncementService.retrieveIntlAnnouncements(country);
    }


}
