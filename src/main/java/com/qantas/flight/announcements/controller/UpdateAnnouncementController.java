package com.qantas.flight.announcements.controller;

import com.qantas.flight.announcements.exceptions.ApplicationException;
import com.qantas.flight.announcements.model.Announcement;
import com.qantas.flight.announcements.service.UpdateAnnouncementService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
public class UpdateAnnouncementController {
    @Autowired
    private UpdateAnnouncementService updateAnnouncementService;

    @ApiOperation(value = "Updates the flight announcements ", httpMethod = "PUT")
    @PutMapping(path = "/v1/update/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateAirportAnnouncements(@RequestBody Announcement announcement,
                                           @RequestParam(value = "channel", required = true) String channel) throws ApplicationException {
        updateAnnouncementService.updateAirportAnnouncements(announcement);

    }
    @ApiOperation(value = "Deletes the broadcast announcements ", httpMethod = "DELETE")
    @DeleteMapping(path = "/v1/delete/{key}",produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteAirportAnnouncements(@PathVariable("key") String key,
                                             @RequestParam(value = "channel", required = true) String channel) throws ApplicationException {
       return updateAnnouncementService.deleteAirportAnnouncements(key);

    }

    @ApiOperation(value = "Retrieves announcements based on key", httpMethod = "GET")
    @GetMapping(path = "/v1/retrieve/{key}")
    public String retrieveStaticAnnouncements(@PathVariable("key") String key,
                                        @RequestParam(value = "channel", required = true) String channel) throws ApplicationException {
        return updateAnnouncementService.retrieveStaticAnnouncements(key);
    }
}