package com.qantas.flight.announcements.exceptions;

public class ApplicationException extends Exception {

    private static final long serialVersionUID = -5703834802939799462L;

    private final ErrorCodes errorCodes;

    public ApplicationException(final ErrorCodes errorCodes, final Throwable throwable) {
        super(errorCodes.getErrorMessage(), throwable);
        this.errorCodes = errorCodes;
    }

    public final ErrorCodes getErrorCodes() {
        return errorCodes;
    }

}
