package com.qantas.flight.announcements.exceptions;

import lombok.Getter;

@Getter
public enum ErrorCodes {

    CONSTRAINT_VIOLATION_EXCEPTION("FLIFO001", 400, "Constraint Violation Exception"),
    SERVER_EXCEPTION("FLIFO002", 500, "Internal Server Error"),
    SYSTEM_EXCPETION("FLIFO003", 500, "Internal System Error"),
    FLIGHT_STATUS_SERVICE_EXCEPTION("FLIFO004", 500, "Flight Status service is unavailable"),
    FLIGHT_STATUS_REQUEST_TIME_OUT_EXCEPTION("FLIFO005", 500, "Request to Flight Status service failed"
            + " due to time out or not able to connect");

    private String errorCode;
    private Integer statusCode;
    private String errorMessage;

    ErrorCodes(final String errorCode, final Integer statusCode, final String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.statusCode = statusCode;
    }
}
