package com.qantas.flight.announcements.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("Flifo Response")
public class FlightResponse {

    private String attempt;
    private TwiMLResponse twiMLResponse;
}
