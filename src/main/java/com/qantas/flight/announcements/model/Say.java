package com.qantas.flight.announcements.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Say {
    private String voice;
    private String loop;
    private String language;
    private String say;

}
