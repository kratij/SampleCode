package com.qantas.flight.announcements.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TwiMLResponse {
    private Say say;
}
