package com.qantas.flight.announcements.rediscache;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;

/**
 * Created by sujatha.bandi on 4/8/17.
 */
@Configuration
@ComponentScan
@EnableCaching
@ConditionalOnProperty(value = "spring.data.redis.repositories.enabled", havingValue = "true")
public class RedisCacheConfig {
    @Value("${spring.redis.host:localhost}")
    private String redisHost;

    @Value("${spring.redis.port:6379}")
    private int redisPort;

    @Bean
    public JedisConnectionFactory redisConnectionFactory() {
        JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory();
        redisConnectionFactory.setHostName(redisHost);
        redisConnectionFactory.setPort(redisPort);
        return redisConnectionFactory;
    }

    @Bean
    public RedisTemplate<?, ?> redisTemplate() {
        RedisTemplate<String, String> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory());
        return redisTemplate;
    }

    @Bean(name="cacheManager")
    public CacheManager cacheManager() {
         RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate());
        cacheManager.setLoadRemoteCachesOnStartup(true);
        cacheManager.setCacheNames(Arrays.asList("Announcements"));
        return cacheManager;
    }
}
