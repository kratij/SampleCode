package com.qantas.flight.announcements.service;

import com.qantas.flight.announcements.model.FlightResponse;
import com.qantas.flight.announcements.model.Say;
import com.qantas.flight.announcements.model.TwiMLResponse;
import com.qantas.flight.announcements.util.AirportUtil;
import com.qantas.flight.announcements.component.AirportDetails;
import com.qantas.flight.announcements.component.RetrieveAnnouncements;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.qantas.flight.announcements.util.Constants.FIRST_LEVEL_MENU1;
import static com.qantas.flight.announcements.util.Constants.FIRST_LEVEL_MENU2;
import static com.qantas.flight.announcements.util.Constants.FIRST_LEVEL_MENU3;
import static com.qantas.flight.announcements.util.Constants.INPUT_TIME_FORMAT;
import static com.qantas.flight.announcements.util.Constants.SPACE;
import static net.logstash.logback.marker.Markers.append;

@Slf4j
@Service
public class AirportAnnouncementService {

    private static final String SYDNEY_FLIGHT_INTRO = "flifo_sydney_flights_intro";
    private static final String BROADCAST_MESSAGE = "flifo_broadcast_message";
    private static final String INTRODUCTION_ANNOUNCEMENT1 = "flifo_introduction_announcement1";
    private static final String FLIGHT_INTRODUCTION_ANNOUNCEMENT1 = "flifo_flight_introduction_announcement1";
    private static final String FLIGHT_ARR_DEP_INTRO_MENU = "flifo_flight_intro_announcement";
    private static final String WELCOME = "welcome";
    private static final String FLIGHT_INTRODUCTION = "flight-introduction";
    private static final String DEPARTURE = "Departure";
    private static final String ARRIVAL = "Arrival";
    private static final String SYDNEY = "SYD";


    @Autowired
    private RetrieveAnnouncements retrieveAnnouncements;

    @Autowired
    AirportDetails airportTimezone;

    public FlightResponse retrieveAirportAnnouncements(String port, String messageCode, String travelType) {
        long processing_start_time = System.currentTimeMillis();
        Say say = new Say();
        FlightResponse flightResponse = new FlightResponse();
        TwiMLResponse twiMLResponse = new TwiMLResponse();
        say.setSay(buildAnnouncement(port, messageCode, travelType));
        twiMLResponse.setSay(say);
        log.info(append("Processing time : ", System.currentTimeMillis() - processing_start_time), null);
        flightResponse.setTwiMLResponse(twiMLResponse);
        return  flightResponse;
    }

    protected String buildAnnouncement(String port, String messageCode, String travelType){
        LocalDateTime localDateTime = airportTimezone.retrieveLocalTime(airportTimezone.retrieveTimezone(port));
        switch(messageCode)
        {
            case WELCOME:
                return retrieveAnnouncements.getProperty(BROADCAST_MESSAGE)+" "+String.format(retrieveAnnouncements.getProperty(INTRODUCTION_ANNOUNCEMENT1), AirportUtil.findAirportName(port), localDateTime.getDayOfWeek().toString(),
                        airportTimezone.returnDayOfMonth(localDateTime.getDayOfMonth()), localDateTime.getMonth().toString()) +
                        SPACE + retrieveAnnouncements.getProperty(FIRST_LEVEL_MENU1) + SPACE + retrieveAnnouncements.getProperty(FIRST_LEVEL_MENU2) +
                        SPACE + retrieveAnnouncements.getProperty(FIRST_LEVEL_MENU3);

            case FLIGHT_INTRODUCTION:
                return String.format(retrieveAnnouncements.getProperty(FLIGHT_INTRODUCTION_ANNOUNCEMENT1),
                        INPUT_TIME_FORMAT.format(localDateTime));

            case DEPARTURE:
                return String.format(retrieveAnnouncements.getProperty(FLIGHT_ARR_DEP_INTRO_MENU),travelType,messageCode)
                        + SPACE + String.format(retrieveAnnouncements.getProperty(FLIGHT_INTRODUCTION_ANNOUNCEMENT1), INPUT_TIME_FORMAT.format(localDateTime))
                        + buildTerminalAnnouncement(port);

            case ARRIVAL:
                return String.format(retrieveAnnouncements.getProperty(FLIGHT_ARR_DEP_INTRO_MENU),travelType,messageCode)
                        + SPACE + String.format(retrieveAnnouncements.getProperty(FLIGHT_INTRODUCTION_ANNOUNCEMENT1),
                        INPUT_TIME_FORMAT.format(localDateTime)) + buildTerminalAnnouncement(port);

        }
        return "";
    }

    protected String buildTerminalAnnouncement(String port)
    {
        return port.equalsIgnoreCase(SYDNEY) ? SPACE + retrieveAnnouncements.getProperty(SYDNEY_FLIGHT_INTRO) : SPACE;
    }
}