package com.qantas.flight.announcements.service;

import com.qantas.flight.announcements.exceptions.ApplicationException;
import com.qantas.flight.announcements.model.FlightResponse;
import com.qantas.flight.announcements.model.Say;
import com.qantas.flight.announcements.model.TwiMLResponse;
import com.qantas.flight.announcements.component.RetrieveAnnouncements;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.qantas.flight.announcements.util.Constants.FIRST_LEVEL_MENU1;
import static com.qantas.flight.announcements.util.Constants.FIRST_LEVEL_MENU2;
import static com.qantas.flight.announcements.util.Constants.FIRST_LEVEL_MENU3;
import static com.qantas.flight.announcements.util.Constants.SPACE;
import static net.logstash.logback.marker.Markers.append;

@Service
@Slf4j
@AllArgsConstructor
public class FlightDomAnnouncementService {


    private static final String FLIGHT_REPEAT_MENU1 = "flifo_flight_repeat_menu1";
    private static final String CLOSING_ANNOUNCEMENT1 = "flifo_closing_announcement1";
    private static final String AU_HASH_MENU ="flifo_domestic_menu_announcement";
    private static final String DOMESTIC_TIME_MENU = "flifo_domestic_menu_time_announcement";
    private static final String DOMESTIC_HOUR_MENU = "flifo_domestic_menu_hour_announcement";
    private static final String INPUT_FLIGHT_MENU = "flifo_enter_flight_menu";
    private static final String MAIN_MENU = "first-level-menu";
    private static final String AU_DOMINT_FLOW = "au-domint-flow";
    private static final String MERIDIEM_MENU = "meridium-menu";
    private static final String HOUR_MENU = "hour-menu";
    private static final String FLIGHT_REPEAT_MENU = "flight-repeat-menu";
    private static final String CLOSING_MESSAGE = "closing-message";
    private static final String INPUT_FLIGHT = "input-flight";

    @Autowired
    private RetrieveAnnouncements retrieveAnnouncements;

    public FlightResponse retrieveDomAnnouncements(String messageCode) throws ApplicationException {
        long processing_start_time = System.currentTimeMillis();
        Say say = new Say();
        TwiMLResponse twiMLResponse = new TwiMLResponse();
        FlightResponse flightResponse = new FlightResponse();
        say.setSay(buildDomesticAnnouncements(messageCode));
        twiMLResponse.setSay(say);
        log.info(append("Processing time : ", System.currentTimeMillis() - processing_start_time), null);
        flightResponse.setTwiMLResponse(twiMLResponse);
        return flightResponse;
    }

    protected String buildDomesticAnnouncements(String messageCode){
        switch (messageCode) {
            case MAIN_MENU:
                return retrieveAnnouncements.getProperty(FIRST_LEVEL_MENU1) + SPACE + retrieveAnnouncements.getProperty(FIRST_LEVEL_MENU2) +
                        SPACE + retrieveAnnouncements.getProperty(FIRST_LEVEL_MENU3);

            case AU_DOMINT_FLOW:
                return retrieveAnnouncements.getProperty(AU_HASH_MENU);

            case MERIDIEM_MENU:
                return retrieveAnnouncements.getProperty(DOMESTIC_TIME_MENU);

            case HOUR_MENU:
                return retrieveAnnouncements.getProperty(DOMESTIC_HOUR_MENU);

            case FLIGHT_REPEAT_MENU:
                return retrieveAnnouncements.getProperty(FLIGHT_REPEAT_MENU1);

            case CLOSING_MESSAGE:
                return retrieveAnnouncements.getProperty(CLOSING_ANNOUNCEMENT1);

            case INPUT_FLIGHT:
                return retrieveAnnouncements.getProperty(INPUT_FLIGHT_MENU);
        }
        return "";
    }
}
