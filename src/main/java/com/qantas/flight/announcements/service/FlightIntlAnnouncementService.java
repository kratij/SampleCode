package com.qantas.flight.announcements.service;

import com.qantas.flight.announcements.component.StatesAirportsComponent;
import com.qantas.flight.announcements.model.FlightResponse;
import com.qantas.flight.announcements.model.Say;
import com.qantas.flight.announcements.model.TwiMLResponse;
import com.qantas.flight.announcements.component.AirportDetails;
import com.qantas.flight.announcements.component.RetrieveAnnouncements;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.qantas.flight.announcements.util.Constants.SPACE;
import static net.logstash.logback.marker.Markers.append;

@Slf4j
@Service
@AllArgsConstructor
public class FlightIntlAnnouncementService {

    private static final String INTL_INTRODUCTION_ANNOUNCEMENT1 = "flifo_international_welcome_announcement";
    private static final String INTL_FIRST_LEVEL_MENU = "flifo_international_first_level_menu";
    private static final String US_AIRPORT_MENU = "flifo_us_airport_menu1";
    private static final String NZ_AIRPORT_MENU = "flifo_nz_airport_menu1";
    private static final String UK_INTRODUCTION = "flifo_uk_introduction";
    private static final String SELECT_AIRPORT_MENU = "flifo_airport_announcement";
    private static final String UK_TIMEZONE = "Europe/London";
    private static final String US = "US";
    private static final String NZ = "NZ";
    private static final String UK = "UK";
    @Autowired
    private RetrieveAnnouncements retrieveAnnouncements;

    @Autowired
    AirportDetails airportTimezone;

    @Autowired
    private StatesAirportsComponent statesAirportsComponent;

    public FlightResponse retrieveIntlAnnouncements(String country){
        long processing_start_time = System.currentTimeMillis();
        Say say = new Say();
        TwiMLResponse twiMLResponse = new TwiMLResponse();
        FlightResponse flightResponse = new FlightResponse();
        say.setSay(buildInternationalAnnouncement(country));
        twiMLResponse.setSay(say);
        log.info(append("Processing time : ", System.currentTimeMillis() - processing_start_time), null);
        flightResponse.setTwiMLResponse(twiMLResponse);
        return flightResponse;
    }

    protected String buildInternationalAnnouncement(String country) {
        switch (country) {
            case US:
                return retrieveAnnouncements.getProperty(INTL_INTRODUCTION_ANNOUNCEMENT1) +
                        SPACE + retrieveAnnouncements.getProperty(SELECT_AIRPORT_MENU) + SPACE + retrieveAnnouncements.getProperty(US_AIRPORT_MENU);

            case NZ:
                return  retrieveAnnouncements.getProperty(INTL_INTRODUCTION_ANNOUNCEMENT1) +
                        SPACE + retrieveAnnouncements.getProperty(SELECT_AIRPORT_MENU) + SPACE + retrieveAnnouncements.getProperty(NZ_AIRPORT_MENU);

            case UK:
                LocalDateTime localDateTime = airportTimezone.retrieveLocalTime(UK_TIMEZONE);
                return String.format(retrieveAnnouncements.getProperty(UK_INTRODUCTION),localDateTime.getDayOfWeek().toString(),
                        airportTimezone.returnDayOfMonth(localDateTime.getDayOfMonth()), localDateTime.getMonth().toString())+ SPACE + retrieveAnnouncements.getProperty(INTL_FIRST_LEVEL_MENU);
        }
        return "";

    }
}
