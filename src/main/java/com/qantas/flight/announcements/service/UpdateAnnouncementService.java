package com.qantas.flight.announcements.service;

import com.qantas.flight.announcements.model.Announcement;
import com.qantas.flight.announcements.component.RetrieveAnnouncements;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class UpdateAnnouncementService {

    @Autowired
    private RetrieveAnnouncements retrieveAnnouncements;

    public void updateAirportAnnouncements(Announcement announcement) {
        retrieveAnnouncements.setProperty(announcement.getKey(), announcement.getMessage());
    }

    public String deleteAirportAnnouncements(String key){
        return retrieveAnnouncements.deleteProperty(key);
    }

    public String retrieveStaticAnnouncements(String key) {
        return retrieveAnnouncements.getProperty(key);
    }
}
