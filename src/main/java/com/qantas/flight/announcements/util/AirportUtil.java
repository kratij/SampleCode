package com.qantas.flight.announcements.util;

import com.qantas.airport.repository.AirportRepository;

public class AirportUtil {

    private static final AirportRepository airportRepository = AirportRepository.getInstance();

    public static final String findAirportName(String airportCode) {

        return airportRepository.findByCode(airportCode) != null ? airportRepository.findByCode(airportCode)
                .defaultDisplayName() : airportCode;
    }
}
