package com.qantas.flight.announcements.util;

import java.time.format.DateTimeFormatter;

public class Constants {

    public static final String FIRST_LEVEL_MENU1 = "flifo_first_level_menu1";
    public static final String FIRST_LEVEL_MENU2 = "flifo_first_level_menu2";
    public static final String FIRST_LEVEL_MENU3 = "flifo_first_level_menu3";
    public static final DateTimeFormatter INPUT_TIME_FORMAT = DateTimeFormatter.ofPattern("hh:mm a");
    public static final String SPACE = " ";

}
